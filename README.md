# README #

### What is this repository for? ###

This is an Alexa (Amazon Echo) skill that let's you ask Alexa for trail statuses around the KC Metro Area.  All data comes from urbantrailco.com website, which also powers the rainoutline too.

Huge thanks to the folks at Urban Trail, https://urbantrailco.com/ for supporting this effort

Version 1.0.1


### How do I get set up? ###

#### Tools
* Node.js - https://nodejs.org/
* npm - https://www.npmjs.com/package/npm  (npm install -g npm)
* Alexa Skill Kit (command line) - https://www.npmjs.com/package/ask-cli  (npm install ask-cli)
* git - https://git-scm.com/downloads

I used Visual Studio Code (https://code.visualstudio.com/), because it makes my life easier

#### Steps
1.  Clone the repo
    ```
    git clone https://kenwiebke@bitbucket.org/kenwiebke/kctrails.git kctrails
    ```
2. Initialize ask-cli
    ```
    cd kctrails
    ask init
    ```
3.  Install packages
    ```
    npm install
    ```
4.  Deploy
    ```
    ask deploy
    ```

### Who do I talk to? ###

Ken Wiebke - kenwiebke@gmail.com
  Strava:  https://www.strava.com/athletes/5525118
  LinkedIn: https://www.linkedin.com/in/kenwiebke/

