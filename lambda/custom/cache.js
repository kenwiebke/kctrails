'use strict';

class cache {
	constructor(cacheDuration) {
		this.cacheDuration = cacheDuration; // in minutes
		this.expiresTime = new Date((new Date()).getTime() - (cacheDuration * 60000));
	}

	get expired() {
		return ((this.cacheItem == null) || (Date.now() > this.expires));
	}

	get data() {
		if (this.expired) {
			this.cacheItem = null;
			return null;
		}
		else {
			return this.cacheItem;
		}
	}

	get expires() {
		return this.expiresTime;
	}

	clear() {
		this.cacheItem	= null;
		let dt = new Date();
		this.expiresTime = new Date(dt.getTime() - (30 * 60000));
	}

	setData(cacheObj) {
		this.cacheItem = cacheObj;
		let dt = new Date();
		this.expiresTime = new Date(dt.getTime() + (this.cacheDuration * 60000));
	}
}

module.exports = cache;