'use strict';
const aws = require('aws-sdk');
const cache = require('./cache.js');
let doc;

aws.config.update({
    region: "us-east-1"
});

const newTableParams = {
    TableName: "UrbanTrailCache",
    AttributeDefinitions: [
        {
            AttributeName: 'id',
            AttributeType: 'S'
        }
    ],
    KeySchema: [
        {
            AttributeName: 'id',
            KeyType: 'HASH'
        }
    ],
    ProvisionedThroughput: {
        ReadCapacityUnits: 5,
        WriteCapacityUnits: 5
    }
};

const keyLookupParams = {
    TableName: newTableParams.TableName,
    ConsistentRead: true,
    Key: {
        "id": "trailCache"
    }
};

class dynamoCache extends cache {
    constructor(cacheDuration) {
        super(cacheDuration);
    }

    setData(cacheObj) {
        super.setData(cacheObj); // keep it in memory
        this._storeInDynamo(cacheObj); // store in DynamoDB
    }

    // override getter to also look at dynamo if not in memory
    get data() {
        let _data = this.cacheItem; // got it in mem
        let that = this; // deal with closures
        return new Promise((resolve, reject) => {
            if (!_data) { // no, check dynamodb
                if (!doc) {
                    doc = new aws.DynamoDB.DocumentClient();
                }
                doc.get(keyLookupParams, function (err, data) {
                    if (err) {
                        console.log('doc.get error: ' + JSON.stringify(err, null, 4));
                        resolve(null);
                    } else {
                        //console.log(JSON.stringify(data));
                        if (data && data.Item && data.Item.data && data.Item.data.trails) {
                            that.expiresTime = new Date(data.Item.expires);
                            that.cacheItem = data.Item.data;
                            if (!that.expired) {
                                console.log("retrieving cache from DynamoDB")
                                resolve(that.cacheItem);
                            } else {
                                resolve(null);
                            }
                        } else {
                            resolve(null);
                        }
                    }
                });
            }
            else {
                if (this.expired) {
                    resolve(null);
                } else {
                    console.log("retrieving cache from memory")
                    resolve(_data);
                }
            }
        });
    }

    _storeInDynamo(cacheObj) {
        if (!doc) {
            doc = new aws.DynamoDB.DocumentClient();
        }

        var params = {
            TableName: newTableParams.TableName,
            Item: {
                "id": "trailCache",
                "expires": super.expires.getTime(),
                "data": cacheObj
            }
        };

        doc.put(params, function (err, data) {
            if (err) {
                console.log('Error during DynamoDB put:' + err);
            }
        })
    }

    _clearCache() {
        //console.log("_clearCache");
        super.clear();
        if (!doc) {
            doc = new aws.DynamoDB.DocumentClient();
        }

        doc.delete(keyLookupParams, function (err, data) {
            if (err) {
                console.log('Error during DynamoDB put:' + err);
            } else {
                //console.log("DynamoDB Delete - done")
            }
        })
    }

    _createTable() {
        if (!this.db) {
            this.db = new aws.DynamoDB();
        }
        this.db.createTable(newTableParams, function (err, data) {
            if (err) {
                console.log('Error creating table: ' + JSON.stringify(err, null, 4));
            }
            console.log('Creating table ' + table + ':\n' + JSON.stringify(data));
        });
    }

}


module.exports = dynamoCache;