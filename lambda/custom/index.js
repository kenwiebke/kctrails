'use strict';

var Alexa = require("alexa-sdk"); // uh, alexa!
var Cache = require('./dynamoCache'); // cahce
var DataService = require('./trailDataService'); // retrieves the data

const WELCOME_MSG = "You can ask for open trails or if a specific trails is open.  ...  Now, what can I help you with?";
const WELCOME_REPROMPT = "For instructions on what you can say, please say help me.";
const HELP_MESSAGE = "You can ask questions such as, what trails are open or what trails are closed, or, you can say exit  ...Now, what can I help you with?";
const HELP_REPROMPT = "You can say things like, is swope open, or you can say exit  ...Now, what can I help you with?";
const APP_ID = 'amzn1.ask.skill.662c391a-c5e5-4bdb-b4cc-68625f96f7a1';
const SKILL_TITLE = "Kansas City MTB Trails Status (data from urbantrailco.com)";
const CARD_IMG = "https://urbantrailco.com/images/template/urban-trail-co-logo.png";
const DASH = " - ";
const NEWLINE = "\r\n";

let cacheDuration = 15;
// bust the cache for testing
if (process.env.CACHE !== 'undefined' && process.env.CACHE == 'false') {
    cacheDuration = 0;
} 
const cache = new Cache(cacheDuration);

//===================================
// magic to wire up to Alexa
//===================================
exports.handler = function (event, context) {
    var alexa = Alexa.handler(event, context);
    // don't set appid for testing
    if ('undefined' === typeof process.env.DEBUG) {
        alexa.appId = APP_ID;
    }
    alexa.registerHandlers(handlers);
    alexa.execute();
};

// Here's the bits that respond to Alexa
var handlers = {
    'LaunchRequest': function () {
        this.response.speak(WELCOME_MSG).listen(WELCOME_REPROMPT);
        this.emit(':responseReady');
    },
    'AllStatusIntent': function () {
        this.emit('ListAllTrailsStatus');
    },
    'OpenTrailsIntent': function () {
        this.emit('ListOpenTrails');
    },
    'ClosedTrailsIntent': function () {
        this.emit('ListClosedTrails');
    },
    'GetTrailStatus': function () {
        this.emit('GetOneTrailStatus');
    },
    'GetOneTrailStatus': function () {
        console.log("GetOneTrailStatus:")
        let trailName = getTrailName(this.event.request.intent.slots).toLowerCase().trim();
        getOneTrailStatus(trailName, (resp, cardResp) => {
            this.response.speak(resp)
                .cardRenderer(SKILL_TITLE, cardResp, getCardImage());
            this.emit(':responseReady');
        });
    },
    'ListAllTrailsStatus': function () {
        console.log("ListAllTrailsStatus:")
        getAllTrailsStatus((resp, cardResp) => {
            this.response.speak(resp)
                .cardRenderer(SKILL_TITLE, cardResp, getCardImage());
            this.emit(':responseReady');
        });
    },
    'ListOpenTrails': function () {
        console.log("ListOpenTrails:")
        getTrailsStatus("open", (resp, cardResp) => {
            this.response.speak(resp)
                .cardRenderer(SKILL_TITLE, cardResp, getCardImage());
            this.emit(':responseReady');
        });
    },
    'ListClosedTrails': function () {
        console.log("ListClosedTrails:")
        getTrailsStatus("closed", (resp, cardResp) => {
            this.response.speak(resp)
                .cardRenderer(SKILL_TITLE, cardResp, getCardImage());
            this.emit(':responseReady');
        });
    },
    'AMAZON.StopIntent': function () {
        this.response.speak('Bye');
        this.emit(':responseReady');
    },
    'AMAZON.HelpIntent': function () {
        this.response.speak(HELP_MESSAGE).listen(HELP_REPROMPT);
        this.emit(':responseReady');
    },
    'AMAZON.CancelIntent': function () {
        this.response.speak('Goodbye');
        this.emit(':responseReady');
    },
    'Unhandled': function () {
        this.response.speak("Sorry, I didn't get that.  " + getHelpText())
            .cardRenderer(SKILL_TITLE, getHelpText());
    }
};



//==========================================================================
// meat of it
//==========================================================================

function getCardImage() {
    let card = {};
    card.smallImageUrl = CARD_IMG;
    card.largeImageUrl = CARD_IMG;
    return card;
}

function getHelpText() {
    return "You can try:'Alexa, ask Urban Trails, what trails are open'" +
        " or 'Alexa, ask Urban Trails if Swope is open'" +
        " or 'Alexa, ask Urban Trails to list all trails"
}

//returns the trail name that the user spoke
function getTrailName(slots) {
    let trailName = slots.TRAIL_NAME.value;

    // let's see if there's a synonym, which is buried pretty deep
    if (slots.TRAIL_NAME &&
        slots.TRAIL_NAME.resolutions &&
        slots.TRAIL_NAME.resolutions.resolutionsPerAuthority[0] &&
        slots.TRAIL_NAME.resolutions.resolutionsPerAuthority[0].status &&
        slots.TRAIL_NAME.resolutions.resolutionsPerAuthority[0].status.code &&
        slots.TRAIL_NAME.resolutions.resolutionsPerAuthority[0].status.code == "ER_SUCCESS_MATCH") {
        trailName = slots.TRAIL_NAME.resolutions.resolutionsPerAuthority[0].values[0].value.name;
    }
    return trailName;
}

function getAllClosedMsg() {
    let resp = "Sorry, all trails are closed; ";
    let msgs = [
        "it is a bad day in Kansas City.",
        "probably time to get another bike.",
        "bummer, you should call your mother.",
        "now what are you going to do?",
        "darn; now what to do?",
        "maybe time to book a flight to somewhere dry and go ride there.",
        "time to clean your bike.",
        "time to clear that honey do list."
    ];
    return resp + msgs[Math.floor(Math.random() * msgs.length)];
}

function getAllOpenMsg() {
    let resp = "All trails are open; ";
    let msgs = [
        "get out there and shred",
        "it is an awesome day in Kansas City",
        "get out and ride!",
        "ride fast and get some air",
        "grip it and rip it!"
    ];
    return resp + msgs[Math.floor(Math.random() * msgs.length)];
}

function getTrailsStatus(status, callback) {
    getData((list) => {
        let matchCount = 0;
        status = status.toLowerCase().trim();
        let resp = `The following trails are ${status}: `;
        let cardResp = status.charAt(0).toUpperCase() + status.slice(1) + " Trails: \r\n"
        try {
            if (list != null) {
                list.forEach(element => {
                    if (element.status.toLowerCase() == status) {
                        matchCount++;
                        resp += element.name + ", "
                        cardResp += matchCount + ") " + element.name + includeIfNotEmpty(DASH, element.description) + ", last updated " + element.lastupdate + NEWLINE;
                    }
                });

                if (matchCount > 0) {
                    resp = resp.substring(0, resp.length - 2);
                }
                if (status === 'open') {
                    resp += "; now get out there and ride!";
                }

                if (status === 'open') {
                    if (matchCount == 0) {
                        resp = getAllClosedMsg();
                        cardResp = resp;
                    } else if (matchCount == list.length) {
                        resp = getAllOpenMsg();
                        cardResp = resp;
                    }
                } else if (status === 'closed') {
                    if (matchCount == 0) {
                        resp = getAllOpenMsg();
                        cardResp = resp;
                    } else if (matchCount == list.length) {
                        resp = getAllClosedMsg();
                        cardResp = resp;
                    }
                }
            }
            else {
                resp += " there was an error retrieving the data."
            }

        } catch (error) {

        }
        callback(resp, cardResp);
    })
}


// handles a SINGLE trail
function getOneTrailStatus(trailName, callback) {
    getData((list) => {
        let resp = "";
        let cardResp = "";
        try {
            let matches = 0;
            if (list != null) {
                for (let element of list) {
                    if (element.name.toLowerCase().trim() == trailName) {
                        matches++;
                        resp = element.name + " is " + element.status + includeIfNotEmpty(".  ", element.description) + "; last updated " + element.lastupdate;
                        cardResp = element.name + " is " + element.status + includeIfNotEmpty(NEWLINE, element.description) + NEWLINE + "Last updated " + element.lastupdate;
                        break;
                    }
                }
                if (matches == 0) {
                    resp = "Sorry, I cannot find the trail " + trailName;
                    cardResp = resp;
                }
            }
            else {
                resp += " there was an error retrieving the data.";
            }
        }
        catch (error) {
            console.log(error);
            resp += "there was an error processing your request.";
        }
        callback(resp, cardResp);
    });
}

// handles ALL trails
function getAllTrailsStatus(callback) {
    getData((list) => {
        let resp = "Here's the trail conditions: ";
        let cardResp = "Trail Status: \r\n"
        try {
            let openCnt = 0;
            let rowNum = 0;
            if (list != null) {
                list.forEach(element => {
                    rowNum++;
                    openCnt += (element.status.toLowerCase() == 'open' ? 1 : 0);
                    resp += element.name + " is " + element.status + ".  "
                    cardResp += rowNum + ") " + element.name + " is " + element.status + ".  Last updated " + element.lastupdate + NEWLINE;
                });
                if (openCnt == 0) {
                    resp = getAllClosedMsg();
                    cardResp = resp;
                } else if (openCnt == list.length) {
                    resp = getAllOpenMsg();
                    cardResp = resp;
                }
            }
            else {
                resp += " there was an error retrieving the data."
            }
        } catch (error) {
            console.log(error);
            resp += "there was an error processing your request."
        }
        callback(resp, cardResp);
    })
}

function includeIfNotEmpty(delim, text) {
    if (!text || text.trim().length == 0) {
        return ""
    } else {
        return delim + text.trim()
    }
}

// first checks cache & if not there, calls the external service
function getData(callback) {
    cache.data
        .then(_cache => {
            if (!_cache || _cache.expired) {
                _callService(callback);
            } else {
                callback(_cache.trails);
            }
        })
        .catch(err => {
            console.log("getData Error: " + err)
            _callService(callback);
        });
}

// call external service to retrieve the data
function _callService(callback) {
    //console.log("..retrieving data");
    let svc = new DataService();
    svc.getTrailData((_dater) => {
        let trailcache = {
            trails: _dater
        };
        // console.log(JSON.stringify(trailcache));
        cache.setData(trailcache);
        callback(trailcache.trails);
    });
}

