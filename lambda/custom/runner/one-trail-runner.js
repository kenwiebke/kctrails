//
// Used for visually testing how things are working
//
const va = require("virtual-alexa");

const alexa = va.VirtualAlexa.Builder()
    .handler("./index.handler") // Lambda function file and name
    .interactionModelFile("../../models/en-US.json")
    .create();

// prevents virtual-alexa from throwing issues with appId mismatch
process.env.DEBUG = true;

// Exercise all intents
// NOTE: synonyms are NOT supported in virtual alexa
console.log("================================================================");
alexa.utter("is Blue River Park open").then((payload) => {
    console.log("OutputSpeech: \r\n" + payload.response.outputSpeech.ssml);
    console.log("\r\nCard: \r\n" + payload.response.card.title + "\r\n" + payload.response.card.text);
});
