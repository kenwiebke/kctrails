//
// Used for visually testing how things are working
//
const va = require("virtual-alexa");
const NEWLINE = "\r\n";

const alexa = va.VirtualAlexa.Builder()
    .handler("./index.handler") // Lambda function file and name
    .interactionModelFile("../../models/en-US.json")
    .create();

// prevents virtual-alexa from throwing issues with appId mismatch
process.env.DEBUG = true;

// Exercise all intents
// NOTE: synonyms are NOT supported in virtual alexa
console.log("================================================================");
alexa.utter("all trails status").then((payload) => {
    console.log("ALEXA SAYS:" + NEWLINE + payload.response.outputSpeech.ssml + NEWLINE);
    console.log("ALEXA CARD:" + NEWLINE + payload.response.card.title + NEWLINE + payload.response.card.text);
}).then(() => {
    console.log("================================================================");
    alexa.utter("open trails").then((payload) => {
        console.log("ALEXA SAYS:" + NEWLINE + payload.response.outputSpeech.ssml+ NEWLINE);
        console.log("ALEXA CARD:" + NEWLINE + payload.response.card.title + NEWLINE + payload.response.card.text);
    }).then(() => {
        console.log("================================================================");
        alexa.utter("closed trails").then((payload) => {
            console.log("ALEXA SAYS:" + NEWLINE + payload.response.outputSpeech.ssml+ NEWLINE);
            console.log("ALEXA CARD:" + NEWLINE + payload.response.card.title + NEWLINE + payload.response.card.text);
        }).then(() => {
            //is {TRAIL_NAME} open"
            console.log("================================================================");
            alexa.utter("is Blue River Park open").then((payload) => {
                console.log("ALEXA SAYS:" + NEWLINE + payload.response.outputSpeech.ssml+ NEWLINE);
                console.log("ALEXA CARD:" + NEWLINE + payload.response.card.title + NEWLINE + payload.response.card.text);
            })
        })
    })
 })

