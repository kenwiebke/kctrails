/*
 * Automated tests to validate responses for the "AllStatus" Intent.   
 * 
 * A few notes to remind me what I was thinking at the time
 * 1.  Stubbed out the "get" operation, so I have predictable results
 * 2.  Added "CACHE" to process.env, so I can force cache to not drive me crazy and do its job
 * 3.  Using mocha-step (replaces It with Step), so tests can run syncronously.  Without this,
 *     I was getting all kinds of weird results due to the async nature of things.
 */

const sinon = require('sinon');         // used to stub out axios.get, so I get predictable results
const axios = require('axios');         // so I can stub you!
const assert = require('assert');       // node.js asserts 
const va = require("virtual-alexa");    // so I can test locally
const cache = require("../dynamoCache");    // so I can test locally
const dater = require("./testdataHelper");

// The link below is where there logic for subbing axios get is from.  
// https://medium.com/@srph/axios-easily-test-requests-f04caf49e057

before(()=> {
    (new cache(0))._clearCache(); // flush cache to ensure dynamo doesn't bust our tests
})

// virtual-alexa config and ready to go.
const alexa = va.VirtualAlexa.Builder()
    .handler("./index.handler") // Lambda function file and name
    .interactionModelFile("../../models/en-US.json")
    .create();

process.env.DEBUG = true;   // prevents virtual-alexa from throwing issues with appId mismatch
process.env.CACHE = false;  // no cache

// my tests
describe('TEST ALL TRAIL STATUS:', function () {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    step("*** validate no 'undefined' in response", () => {
        // assemble
        let data = dater.cloneTestData();
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("all trails status").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            assert.equal(true, ssml.length > 0);
            assert.equal(-1, ssml.indexOf("undefined"));

            assert.equal(true, cardtitle.length > 0);
            assert.equal(-1, cardtitle.indexOf("undefined"));

            assert.equal(true, cardtext.length > 0);
            assert.equal(-1, cardtext.indexOf("undefined"));
        })
    })
    
    step("*** validate all open response", () => {
        let data = dater.getAllOpenTrails();
        const resolved = new Promise((r) => r({ data }));
        sandbox.stub(axios, 'get').returns(resolved);

        return alexa.utter("all trails status").then((payload) => {
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            assert.equal(true, ssml.length > 0);
            assert.ok(ssml.indexOf("All trails are open") > 0);

            assert.equal(true, cardtitle.length > 0);
            assert.equal(-1, cardtitle.indexOf("undefined"));

            assert.equal(true, cardtext.length > 0);
            assert.ok(cardtext.indexOf("All trails are open") >= 0);
        })
    })

    step("*** validate all closed response", () => {
        let data = dater.getAllClosedTrails();
        const resolved = new Promise((r) => r({ data }));
        sandbox.stub(axios, 'get').returns(resolved);

        return alexa.utter("all trails status").then((payload) => {
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            assert.equal(true, ssml.length > 0);
            assert.ok(ssml.indexOf("all trails are closed") > 0);

            assert.equal(true, cardtitle.length > 0);
            assert.equal(-1, cardtitle.indexOf("undefined"));

            assert.equal(true, cardtext.length > 0);
            assert.ok(cardtext.indexOf("all trails are closed") >= 0);
        })
    })

    step("*** validate one open and two closed", () => {
        let data = dater.getAllClosedTrails();
        dater.setTrailStatus(data[dater.WYCO],dater.STATUS_OPEN);
        const dataResolved = new Promise((r) => r({ data }));
        sandbox.stub(axios, 'get').returns(dataResolved);

        return alexa.utter("all trails status").then((payload) => {
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            assert.ok(ssml.indexOf("Blue River Park is Closed") > 0);
            assert.ok(ssml.indexOf("Shawnee Mission Park is Closed") > 0);
            assert.ok(ssml.indexOf("Wyandotte County Lake is Open") > 0);

            assert.equal(true, cardtitle.length > 0);
            assert.equal(-1, cardtitle.indexOf("undefined"));

            assert.ok(cardtext.indexOf("Blue River Park is Closed") > 0);
            assert.ok(cardtext.indexOf("Shawnee Mission Park is Closed") > 0);
            assert.ok(cardtext.indexOf("Wyandotte County Lake is Open") > 0);
        })
    })
    
})

