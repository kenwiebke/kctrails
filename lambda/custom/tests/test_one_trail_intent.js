/*
 * Automated tests to validate responses for the "OneStatus" Intent.   
 * 
 * See test_all_status_intent.js for more details about the tests, etc.
 */

const sinon = require('sinon');         // used to stub out axios.get, so I get predictable results
const axios = require('axios');         // so I can stub you!
const assert = require('assert');       // node.js asserts 
const va = require("virtual-alexa");    // so I can test locally
const cache = require("../dynamoCache");    // so I can test locally
const dater = require("./testdataHelper");

// virtual-alexa config and ready to go.
const alexa = va.VirtualAlexa.Builder()
    .handler("./index.handler") // Lambda function file and name
    .interactionModelFile("../../models/en-US.json")
    .create();

process.env.DEBUG = true;   // prevents virtual-alexa from throwing issues with appId mismatch
process.env.CACHE = false;  // no cache

before(()=>{
    (new cache(0))._clearCache(); // flush cache to ensure dynamo doesn't bust our tests
})

// my tests
describe('TEST ONE TRAILSSTATUS:', function () {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    step("*** Validate Closed Status", () => {
        // assemble
        let data = dater.getAllOpenTrails();
        dater.setTrailStatus(data[dater.BLUERIVER], dater.STATUS_CLOSED);
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("is Blue River Park open").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            // validate speach
            assert.ok(ssml.indexOf("Blue River Park is Closed") > 0);
            assert.equal(-1, ssml.indexOf("undefined"));
            // validate card
            assert.ok(cardtitle.indexOf("Kansas City MTB Trails Status") >= 0);
            assert.ok(cardtext.indexOf("Blue River Park is Closed") >= 0);
        })
    })

    step("*** Validate Open Status", () => {
        // assemble
        let data = dater.getAllOpenTrails();
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("is Blue River Park open").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            // validate speach
            assert.equal(-1, ssml.indexOf("undefined"));
            assert.ok(ssml.indexOf("Blue River Park is Open") > 0);
            // validate card
            assert.ok(cardtitle.indexOf("Kansas City MTB Trails Status") >= 0);
            assert.ok(cardtext.indexOf("Blue River Park is Open") >= 0);
        })
    })

    step("*** Validate Bogus Status", () => {
        // assemble
        let data = dater.getAllOpenTrails();
        dater.setTrailStatus(data[dater.BLUERIVER], dater.STATUS_BOGUS);
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("is Blue River Park open").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            // validate speach
            assert.equal(-1, ssml.indexOf("undefined"));
            assert.ok(ssml.indexOf("Blue River Park is Delayed") > 0);
            // validate card
            assert.ok(cardtitle.indexOf("Kansas City MTB Trails Status") >= 0);
            assert.ok(cardtext.indexOf("Blue River Park is Delayed") >= 0);
        })
    })
})
