/*
 * Automated tests to validate responses for the "OneStatus" Intent.   
 * 
 * See test_all_status_intent.js for more details about the tests, etc.
 */

const sinon = require('sinon');         // used to stub out axios.get, so I get predictable results
const axios = require('axios');         // so I can stub you!
const assert = require('assert');       // node.js asserts 
const va = require("virtual-alexa");    // so I can test locally
const cache = require("../dynamoCache");    // so I can test locally
const dater = require("./testdataHelper");

// virtual-alexa config and ready to go.
const alexa = va.VirtualAlexa.Builder()
    .handler("./index.handler") // Lambda function file and name
    .interactionModelFile("../../models/en-US.json")
    .create();

process.env.DEBUG = true;   // prevents virtual-alexa from throwing issues with appId mismatch
process.env.CACHE = false;  // no cache

before(() => {
    (new cache(0))._clearCache(); // flush cache to ensure dynamo doesn't bust our tests
})

// my tests
describe('TEST OPEN TRAILSSTATUS:', function () {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
        sandbox.restore();
    });

    step("*** Validate Open Status - not all trails open", () => {
        // assemble
        let data = dater.getAllOpenTrails();
        dater.setTrailStatus(data[dater.BLUERIVER], dater.STATUS_CLOSED);
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("open trails").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            // validate speach
            assert.ok(ssml.indexOf("The following trails are open:") >= 0);
            assert.ok(ssml.indexOf("Shawnee Mission Park") >= 0);
            assert.ok(ssml.indexOf("Wyandotte County Lake") >= 0);
            assert.equal(-1, ssml.indexOf("undefined"));
            // validate card
            assert.ok(cardtitle.indexOf("Kansas City MTB Trails Status") >= 0);
            assert.ok(cardtext.indexOf("Open Trails:") >= 0);
            assert.ok(cardtext.indexOf("Shawnee Mission Park") >= 0);
            assert.ok(cardtext.indexOf("Wyandotte County Lake") >= 0);
        })
    })



    step("*** Validate ALL-OPEN Status", () => {
        // assemble
        let data = dater.getAllOpenTrails();
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("open trails").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            // validate speach
            assert.equal(true, ssml.length > 0);
            assert.ok(ssml.indexOf("All trails are open") > 0);

            // validate card
            assert.equal(true, cardtitle.length > 0);
            assert.equal(-1, cardtitle.indexOf("undefined"));

            assert.equal(true, cardtext.length > 0);
            assert.ok(cardtext.indexOf("All trails are open") >= 0);
        })
    })
    step("*** Validate ALL-CLOSED Status", function() {
        let data = dater.getAllClosedTrails();
        const dataResolved = new Promise((r) => r({ data })); // fake the data; don't call external service
        sandbox.stub(axios, 'get').returns(dataResolved);

        // act
        return alexa.utter("open trails").then((payload) => {
            // assert
            let ssml = payload.response.outputSpeech.ssml;
            let cardtitle = payload.response.card.title
            let cardtext = payload.response.card.text
            // console.log("ssml: " + ssml)
            // console.log("card: " + cardtext)

            // validate speach
            assert.equal(true, ssml.length > 0);
            assert.ok(ssml.indexOf("Sorry, all trails are closed") > 0);

            // validate card
            assert.equal(true, cardtitle.length > 0);
            assert.equal(-1, cardtitle.indexOf("undefined"));

            assert.equal(true, cardtext.length > 0);
            assert.ok(cardtext.indexOf("all trails are closed") >= 0);
        })
    })


})
