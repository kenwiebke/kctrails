'use strict';

/*
 * Just test data helper stuff
 */

// trail status constants
const STATUS_OPEN = "0";
const STATUS_OPEN_TXT = "Open";
const STATUS_DELAYED = "1";
const STATUS_DELAYED_TXT = "Delayed";
const STATUS_CLOSED = "2";
const STATUS_CLOSED_TXT = "Closed";
const STATUS_BOGUS = "9";
const STATUS_BOGUS_TXT = "Bogus";
// other constatnts
const STATUS_COL = 3;
const STATUS_TXT_COL = 6;

const testData = [
      ["9132040204", "1", "Blue River Park", STATUS_CLOSED, "1522521868", STATUS_CLOSED_TXT, " ", "5"]
    , ["9132040204", "11", "Shawnee Mission Park", STATUS_OPEN, "1522516445", "A little soft in places but drying nicely.", STATUS_OPEN_TXT, "6"]
    , ["9132040204", "15", "Wyandotte County Lake", STATUS_OPEN, "1522503184", "Firm with a few soft spots", STATUS_OPEN_TXT, "5"]
];
      
function cloneTestData() {
    return JSON.parse(JSON.stringify(testData)); // clone data
}

function getAllOpenTrails()
{
    let dater = cloneTestData() 
    dater.forEach(element => {
        setTrailStatus(element, STATUS_OPEN)
    });
    return dater;
}

function getAllClosedTrails()
{
    let dater = cloneTestData(); 
    dater.forEach(element => {
        setTrailStatus(element, STATUS_CLOSED)
    });
    return dater;
}

function setTrailStatus(datarow, status)
{
    switch(status) {
        case STATUS_OPEN: {
            datarow[STATUS_COL] = STATUS_OPEN;
            datarow[STATUS_TXT_COL] = STATUS_OPEN_TXT;
            break;
        }
        case STATUS_CLOSED: {
            datarow[STATUS_COL] = STATUS_CLOSED;
            datarow[STATUS_TXT_COL] = STATUS_CLOSED_TXT;
            break;
        }
        case STATUS_BOGUS: {
            datarow[STATUS_COL] = STATUS_BOGUS;
            datarow[STATUS_TXT_COL] = STATUS_BOGUS_TXT;
            break;
        }
        default: {
            datarow[STATUS_COL] = STATUS_DELAYED;
            datarow[STATUS_TXT_COL] = STATUS_DELAYED_TXT;
        }
    }
}



module.exports = {
    cloneTestData,
    getAllOpenTrails,
    getAllClosedTrails,
    setTrailStatus,
    STATUS_BOGUS,
    STATUS_CLOSED,
    STATUS_DELAYED,
    STATUS_OPEN,
    BLUERIVER: 0,
    SHAWNEE:1,
    WYCO:2
};