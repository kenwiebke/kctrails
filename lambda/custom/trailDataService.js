const ta = require("time-ago")
const axios = require("axios")

class trailDataService {
    constructor() {
        this.trails = [];
        this.statusVal = "OK";
        this.statusCodeVal = "0";
        this.err = "";
    }

    getTrailData(callback) {
        axios
            .get('http://a.statushare.com/embed/1/builder1.php?number=9132040204&output=json')
            .then(response => {
                // console.log(response.data);
                this.err = "";
                this.statusCodeVal = response.status;
                this.statusVal = response.statusText;
                this._parseResponseData(response.data);
                this.trails.sort(this._compare);
                callback(this.trailData);
            })
            .catch(error => {
                console.log(error);
                this.err = error.message;
                try {
                    this.statusVal = error.response.statusText;
                    this.statusCoeVal = error.response.status;
                } catch (err) {
                    console.log(err);
                }
                callback(null);
            });
    }
    
    _parseResponseData(data)
    { // dynamodb doesn't like empty strings, so we fake a space if necessary
        data.forEach(element => {
            let trail = {};
            trail.name = this._ensureNotEmpty(element[2])
            trail.status = this._ensureNotEmpty(this._parseStatus(element[3], element[6]))
            trail.description = this._ensureNotEmpty(element[5])
            trail.lastupdate =  this._getTimeAgo(element[4])
            this.trails.push(trail);
        });
    }
    
    _getTimeAgo(nbr)
    {
        let parsed = parseInt(nbr);
        if (isNaN(parsed)) {
            return "not available"
        }
        return ta.ago(parsed * 1000)
    }

    _ensureNotEmpty(s)
    {
        return ((!s || 0 === s.length) ? " " : s)
    }

    _compare(a,b)
    {
        const a1 = a.name.toUpperCase()
        const b1 = b.name.toUpperCase();
    
        let comp = 0
        if (a1 > b1) {
            comp = 1
        } else if (b1 > a1) {
            comp = -1;
        }
        return comp;
    }    
    
    _parseStatus(statusVal, statusTxt)
    {
        if (statusTxt && statusTxt.trim().length > 0)
        {
            let status = statusTxt.trim().toLowerCase()
            switch(status) {
                case "open": return "Open"
                case "closed": return "Closed"
                default: return "Delayed"
            }
            return statusTxt
        } else {
            let num = parseInt(statusVal);
            switch(num) {
                case 0: return "Open"
                case 2: return "Closed"
                default: return "Delayed"
            }
        }
    }

    get status() {
        return this.statusVal;
    }

    get trailData(){
        return this.trails;
    }
    get statusCode() {
        return this.statusCodeVal;
    }

    get error() {
        return this.err;
    }
}

module.exports = trailDataService